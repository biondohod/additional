#pragma once
namespace ds
{
	class Quad
	{
	public:
		Quad(int side);
		~Quad();
		int CalculateSquare();
		int CalculatePerimeter();
			
	private:
		double m_side;
	};
}