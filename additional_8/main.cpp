#include <iostream>
#include "figure.hpp"
int main()
{
	ds::Quad figure(5);
	
	std::cout << "Square of quad = " << figure.CalculateSquare() << std::endl;
	std::cout << "Perimeter of quad = " << figure.CalculatePerimeter() << std::endl;
	return 0;
}