#include <iostream>
#include "figure.hpp"
namespace ds
{
	
		Quad::Quad(int side)
		{
			m_side = side;
			//std::cout << " Constructor is works! side = " << side << std::endl;
			if (m_side <= 0)
			{
				std::cout << "Error! The side of quad can't be less or equal than 0. ";
				if (m_side < 0)
				{
					std::cout << "Now side equal modulus of the side(" << abs(side) << ")" << std::endl;
					m_side = abs(m_side);
				}
				else
				{
					std::cout << "Now side equal 1" << std::endl;
					m_side = 1;
				}
				
			}
		}
		Quad::~Quad()
		{
			//std::cout << " Destructor is works!";
		}
		int Quad::CalculateSquare()
		{
			return (m_side * m_side);
		}
		int Quad::CalculatePerimeter()
		{
			return (4 * m_side);
		}
	
}