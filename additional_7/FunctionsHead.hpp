#pragma once
#include <vector>
#include <iostream>
const int length = 10000;
const int N = length * 2;
namespace ds
{
	void InputSequence(std::vector<int> sequence, int& len);
	void OutputSequence(std::vector<int> sequence);
}
