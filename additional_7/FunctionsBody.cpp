#include "FunctionsHead.hpp"
#include <vector>
#include <iostream>
namespace ds
{
	void InputSequence(std::vector<int> sequence, int& len)
	{
		std::cout << "Input your sequence" << std::endl;
		for (int i=0; i<len; i++)
		{
			int num;
			std::cin >> num;
			int num_temp = num;
			int multdigit = 1;
			int count7 = 0;
			int count0 = 0;
			while (num_temp > 0)
			{
				multdigit = multdigit * (num_temp % 10);
				if (num_temp % 10 == 7)
					count7++;
				else if (num_temp % 10 == 0)
					count0++;
				num_temp = num_temp / 10;
			}
			if (multdigit == 18)
			{
				if (i > 0)
				{
					i = i - 1;
					len = len - 1;
				}
				else
					i = 0;
			}
			else
			{
				if (count7 > 0 && count0 == 0)
				{
					sequence.push_back(num);
					sequence.push_back(num);
					i++;
					len++;
				}
				else 
					sequence.push_back(num);
			}
		}
		std::cout << "Your processed sequence is :" << std::endl;
		std::cout << "[ ";
		for (int i = 0; i < sequence.size(); i++)
			std::cout << sequence[i] << " ";
		std::cout << "]";
	}

	void OutputSequence(std::vector<int> sequence)
	{
		std::cout << "Your processed sequence is :" << std::endl;
		std::cout << "[ ";
		for (int i=0; i<sequence.size(); i++)
				std::cout << sequence[i] << " ";
		std::cout << "]";
	}
}
