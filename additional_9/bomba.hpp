#pragma once
#include <iostream>
namespace ds
{
	class bomba
	{
	public:
		bomba(int size);
		~bomba();
		void Create();
		int GetSize();
		void PushBack(int num);
		void PushByIndex(int num, int index);
		int GetByIndex(int index);
		void DeleteByIndex(int index);
		void Output();
	private:
		int m_size;
		int indPushBack=0;
		int* bombaArr;
		
	};
}