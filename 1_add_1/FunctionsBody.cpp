#include <iostream>
#include "FunctionsHead.hpp"
namespace ds
{
	void InputData(int data[])
	{
		std::cout << "Input 'Q1', 'P1, 'Q2', 'P2, A separated by a space" << std::endl;
		for (int i = 0; i < N; i++)
			std::cin >> data[i];
	}
	int Processing(int data[])
	{
        int lkw_num[2];

        if (data[0] > data[2]) {
            lkw_num[0] = 2;
            lkw_num[1] = 0;
        }
        else {
            lkw_num[0] = 0;
            lkw_num[1] = 2;
        }

        int prise = 0;
        int weight = data[4];

        while ((weight - data[lkw_num[1]]) > 0) {
            weight -= data[lkw_num[1]];
            prise += data[lkw_num[1] + 1];
        }

        int maxLKW_prise = prise + data[lkw_num[1] + 1];

        while (weight > 0) {
            weight -= data[lkw_num[0]];
            prise += data[lkw_num[0] + 1];
        }

        if (maxLKW_prise < prise)
            return maxLKW_prise;
        else
            return prise;
    }


}