#include <iostream>
#include <string>
#include "FunctionsHead.hpp"
namespace ds
{
	
	std::string InputString(std::string& mainString)
	{
		std::cout << "Input your string. Possible number of characters from 0 to 100" << std::endl;
		std::cin >> mainString;
		while (mainString.length() > 100 || mainString.length()<1)
		{
			std::cout << "Error! length of string must be exceed 0 and must not exceed 100. Try again" << std::endl;
			std::cin >> mainString;
		}
		return mainString;
	}	
	void ProcessingString(std::string mainString)
	{
		for (int i = 0; i < mainString.length(); i++)
		{
			unsigned char symbol = mainString[i];
			if (int(symbol) >= 48 && int(symbol) <= 57)
				std::cout << "Digit " << symbol << std::endl;
			else if ((int(symbol) >= 65 && int(symbol) <= 90) || (int(symbol) >= 97 && int(symbol) <= 122))
				std::cout << "Letter(english) " << symbol << std::endl;
			else if ((int(symbol) >= 192 && int(symbol) <= 255))
				std::cout << "Letter(russian) " << symbol << std::endl;
			else if (int(symbol) == 33 || int(symbol) == 34 || int(symbol) == 44 || int(symbol) == 45 || int(symbol) == 46 || int(symbol) == 58 || int(symbol) == 59 || int(symbol) == 63)
				std::cout << "Punctuation marks " << symbol << std::endl;
			else
				std::cout << "Other symbols " << symbol << std::endl;
		}
	}

}